import Header from './components/Header';
import Main from './TestComponents/Main';
import Basket from './TestComponents/Basket';
import ProductsLoginNav from "./components/ProductsLoginNav"

import { useState, useEffect } from 'react';
const storageCart = JSON.parse(localStorage.getItem("cartItems"));





export default function AppTest() {
const [cartItems, setCartItems] = useState([]);





const [products, setProducts] = useState([])


  useEffect(() => {
  fetch("http://localhost:4000/api/product/products")
  .then(res => res.json())
  .then(data => {
    console.log(data)




setProducts(data)

  })
}, []);




useEffect(() => {
localStorage.setItem("cartItems", JSON.stringify(cartItems));
}, [cartItems])





  const onAdd = (product) => {
    const exist = cartItems.find((x) => x._id === product._id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x._id === product._id ? { ...exist, quantity: exist.quantity + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, quantity: 1 }]);
    }
  };
  const onRemove = (product) => {
    const exist = cartItems.find((x) => x._id === product._id);
    if (exist.quantity === 1) {
      setCartItems(cartItems.filter((x) => x._id !== product._id));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x._id === product._id ? { ...exist, quantity: exist.quantity - 1 } : x
        )
      );
    }
  };
  return (
    <div className="App">
      <ProductsLoginNav />
      <div className="row">
        <Main products={products} onAdd={onAdd}></Main>
        <Basket
          cartItems={cartItems}
          onAdd={onAdd}
          onRemove={onRemove}
        ></Basket>
      </div>
    </div>
  );
}

