import React from 'react';
import { Row, Col, Card } from "react-bootstrap";



export default function Product(props) {
  const { product, onAdd } = props;
  return (
     <Col xs={12} md={4} className="mt-5">
     <Card className="cardHighlight p-3">
      <img className="small" src={product.imageUrl} alt={product.name} />
      <h3 className="text-center">{product.name}</h3>
      <div className="text-center">Price: PHP {product.price}</div>
      <div>
        <button onClick={() => onAdd(product)} className="text-white">Add To Cart</button>
      </div>
      </Card>
      </Col>


    
  );
}