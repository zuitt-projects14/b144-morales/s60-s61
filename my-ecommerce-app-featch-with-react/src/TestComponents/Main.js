import React from 'react';
import Product from './Product';
import { Row, Col, Card } from "react-bootstrap";


export default function Main(props) {
  const { products, onAdd } = props;
  return (
      
        <Row className="mt-3 mb-3 container-fit justify-content-center">
      <h1 className="text-white text-center">Products</h1>
      
        {products.map((product) => (
          <Product key={product.id} product={product} onAdd={onAdd}></Product>
        ))}
        
    </Row>

    

  );
}
