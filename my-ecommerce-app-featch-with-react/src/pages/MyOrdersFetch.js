import { Fragment, useEffect, useState } from "react";
import MyOrdersCard from "../components/MyOrdersCard";
import { Row, Col, Card } from "react-bootstrap";
import ProductsLoginNav from "../components/ProductsLoginNav"
import Container from 'react-bootstrap/Container';
import Footer from "../components/Footer"


export default function MyOrdersFetch(){

const [orders, setOrders] = useState([]);
const [email, setEmail] = useState("dawn@email.com");




/*useEffect(() => {
	fetch("http://localhost:4000/api/order/users/orders")
	.then(res => res.json())
	.then(data => {
		console.log(data)

		



setOrders(data.map(order => {
	return (
	
		

		<MyOrdersCard key={order._id} orderProp={order} />
		
     
	)
})
)

	})
}, []);*/










useEffect(() => {
	fetch("http://localhost:4000/api/order/users/myOrders", {
		method: "GET",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		

setOrders(data.map(order => {
	return (
	
		

		<MyOrdersCard key={order._id} orderProp={order} />
		
     
	)
})
)





	})
}, []);




	return (
		<Fragment>
		<ProductsLoginNav />
		<Row className="mt-3 mb-3 container-fit justify-content-center">
			
			{orders}
			</Row>
			<Footer />
		</Fragment>
     
		

		)
}