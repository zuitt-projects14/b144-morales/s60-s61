import { Fragment, useEffect, useState, useContext } from "react";
import ProductsLoginCard from "../components/ProductsLoginCard";
import { Row, Col, Card } from "react-bootstrap";
import ProductsLoginNav from "../components/ProductsLoginNav"
/*import Container from 'react-bootstrap/Container';*/
import Footer from "../components/Footer"
import { Form, Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';

import Header from "../components/Header"




export default function ProductsLogin(){

const [products, setProducts] = useState();


useEffect(() => {
	fetch("http://localhost:4000/api/product/allActive")
	.then(res => res.json())
	.then(data => {
		

		//Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components

setProducts(data.map(product => {
	return (
		<ProductsLoginCard key={product._id}  productProp={product} />

		
	)
})
)
		



	})
}, []);




	return (

		
		<Fragment>
		
		<ProductsLoginNav />
	
		<Row className="mt-3 mb-3 container-fit justify-content-center">
						
		
{products}
			
			</Row>

			
			<Footer />
			
		</Fragment>


     
		

		)
}