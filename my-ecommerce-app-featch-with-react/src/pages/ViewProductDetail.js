import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import ViewProductLogin from "../components/ViewProductLogin";
import ProductsLoginNav from "../components/ProductsLoginNav";
import Footer from "../components/Footer";
import { Fragment } from 'react';


export default function Login(){
    return (
        <Fragment>
        <ProductsLoginNav />
        <ViewProductLogin />
        <Footer />
        </Fragment>

        )
}


