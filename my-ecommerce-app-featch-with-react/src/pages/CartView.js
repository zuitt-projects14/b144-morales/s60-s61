

import { Redirect } from 'react-router-dom';
import ProductsLoginNav from "../components/ProductsLoginNav"
import Basket from "../TestComponents/Basket";
import Footer from "../components/Footer";
import { Fragment } from 'react';


export default function CartView(){
	return (
			<Fragment>
				<ProductsLoginNav />
				<Basket />
				<Footer />
			</Fragment>

		)
}