import { useState } from "react";
import { loadStripe } from "@stripe/stripe-js"

const stripePromise = loadStripe("pk_test_51KIPpzH68q6QTForzb9SjxyCLfDDSBJb4hTzwRsnKqg2HkFwBCzVoI15OeSFsr9DC86vDXROvqq9csnaqGeDX3qk00DnnauvXv");



const IndexCheckout = () => {
const [stripeError, setStripeError] = useState();
const [loading, setLoading] = useState();


const handleClick = async () => {
	setLoading(true);

const stripe = await stripePromise;



const { error } = await stripe.redirectToCheckout({
lineItems: [{
	price: "price_1KIPywH68q6QTForZTlyrfEM",
	quantity: 1,
},],

mode: "payment",
cancelUrl: window.location.origin,
successUrl: `${window.location.origin}/thankyou`,
customerEmail: "afternoon@mail.com"
});


if (error){
	setLoading(false);
	setStripeError(error);
}
};

return (
<>

{ stripeError && <p style={{color: "red" }}>{stripeError} </p>}
<button role="link" onClick={handleClick} disabled={loading}>Go to checkout
</button>


</>
)
};


export default IndexCheckout;