import react from "react"
import Footer from "../components/Footer";
import ThankYouNav from "../components/ThankYouNav";
import Swal from "sweetalert2";
import { Row, Col, Card } from "react-bootstrap";
import Slogan from "../components/Slogan"




export default function ThankYou(){
return(
		<>
			<ThankYouNav />
			<Slogan />
			<Col xs={12} md={12} className="mt-5">
          <Card className="cardHighlight2 p-3">
          <h1 className="text-center">Congratulations!</h1>
          <h2 className="text-center">You Successfully place an order.</h2>
			<h3 className="text-center">Thank you for trusting us.</h3>
			
			</Card>
			</Col>
			<Footer />
		</>
	)

}