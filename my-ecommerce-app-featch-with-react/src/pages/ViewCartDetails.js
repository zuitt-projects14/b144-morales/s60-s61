import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import CartView from "../components/CartView";
import ProductsLoginNav from "../components/ProductsLoginNav";
import Footer from "../components/Footer";
import { Fragment } from 'react';


export default function Login(){
    return (
        <Fragment>
        <ProductsLoginNav />
        <CartView />
        <Footer />
        </Fragment>

        )
}