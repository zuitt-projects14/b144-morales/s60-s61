import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import LoginComp from "../components/LoginComp";
import NavLogin from "../components/NavLogin";
import Footer from "../components/Footer";
import { Fragment } from 'react';


export default function Login(){
    return (
        <Fragment>
        <NavLogin />
        <LoginComp />
        <Footer />
        </Fragment>

        )
}