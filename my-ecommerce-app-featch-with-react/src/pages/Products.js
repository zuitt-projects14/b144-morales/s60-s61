import { Fragment, useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";
import { Row, Col, Card } from "react-bootstrap";
import AllProductNav from "../components/AllProductNav"
import Container from 'react-bootstrap/Container';
import Footer from "../components/Footer"


export default function Products(){
//Checks to see if the the mock data was captured
/*console.log(courses);
console.log(courses[0]);*/



//State that will be used to store the courses retrieved from the database
const [products, setProducts] = useState([])



// Retrieve the courses from the database upon intial render of the "Courses" component

useEffect(() => {
	fetch("http://localhost:4000/api/product/allActive")
	.then(res => res.json())
	.then(data => {
		console.log(data)

		//Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components


setProducts(data.map(product => {
	return (
	
		

		<ProductCard key={product._id} productProp={product} />
		
     
	)
})
)

	})
}, []);




	return (
		<Fragment>
		<AllProductNav />
		<Row className="mt-3 mb-3 container-fit justify-content-center">
			
			{products}
			</Row>
			<Footer />
		</Fragment>
     
		

		)
}