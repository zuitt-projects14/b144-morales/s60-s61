import Navbar from "react-bootstrap/Navbar";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Button from "react-bootstrap/Button"

import { Link, NavLink } from 'react-router-dom';


export default function ThankYouNav(props){
	return (
		
  <Navbar expand="lg" className="nav navbar navbar-dark mx-auto">
  <Container className="mx-auto">
    <Navbar.Brand as={NavLink} to="//api/products/login/cart" className="text-white" exact>MATHWEB</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mx-auto">
        <Nav.Link as={NavLink} to="/productslogin" className="text-white mx-5" exact>HOME</Nav.Link>
        <Nav.Link as={NavLink} to="/MyOrdersTest" className="text-white mx-5" exact>MY ORDERS</Nav.Link>
        
        
        <Nav.Link as={NavLink} to="/logout" className="text-white mx-5" exact>LOGOUT</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>

		)
}