import { Fragment, useEffect, useState, useContext } from "react";
import AdminProductsCard from "../adminComponents/AdminProductsCard";
import { Row, Col, Card } from "react-bootstrap";
import AdminPageAfterLoginViewNav from "../adminNav/AdminPageAfterLoginViewNav"
/*import Container from 'react-bootstrap/Container';*/
import AdminFooter from "../AdminFooter"
import { Form, Button, Container } from 'react-bootstrap';
import UserContext from '../UserContext';






export default function AdminProductsLogin(){

const [products, setProducts] = useState();


useEffect(() => {
	fetch("http://localhost:4000/api/product/allActive")
	.then(res => res.json())
	.then(data => {
		

		//Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components

setProducts(data.map(product => {
	return (
		<AdminProductsCard key={product._id}  productProp={product} />

		
	)
})
)
		



	})
}, []);




	return (

		
		<Fragment>
		
		
	
		<Row className="mt-3 mb-3 container-fit justify-content-center">
						
		
{products}
			
			</Row>

			
			<AdminFooter />
			
		</Fragment>


     
		

		)
}