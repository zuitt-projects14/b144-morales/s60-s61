import Button  from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { Row, Col, Card } from "react-bootstrap";

export default function AllOrdersCard({orderProp}){

const {_id, product, name, description, price, purchasedOn, quantity} = orderProp;


 return (


          <Card className="cardHighlight p-3">
          
    <MDBTable>
      <MDBTableHead>
        <tr className="">
          <th className="">ProductId</th>
          <th className="">Quantity</th>
          <th className="">purchasedOn</th>
          <th className="">Price</th>
        </tr>
      </MDBTableHead>
      <MDBTableBody className="">
        <tr className="">
          
          <td>{product}</td>
          
          <td>{quantity}</td>
          <td>{purchasedOn}</td>
          <td>{price}</td>
        </tr>
        
      </MDBTableBody>
      
    </MDBTable>

</Card>

    
		)    
}










  
