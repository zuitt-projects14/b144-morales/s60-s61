import Navbar from "react-bootstrap/Navbar";
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Button from "react-bootstrap/Button"

import { Link, NavLink } from 'react-router-dom';


export default function AdminPageAfterLoginNav(){
	return (
		
  <Navbar expand="lg" className="nav navbar navbar-dark mx-auto">
  <Container className="mx-auto">
    <Navbar.Brand as={NavLink} to="/AdminPageAfterLogin" className="text-white" exact>MATHWEB</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mx-auto">
        <Nav.Link as={NavLink} to="/AdminPageAfterLogin" className="text-white mx-5" exact>HOME</Nav.Link>
        <Nav.Link as={NavLink} to="/adminproducts" className="text-white mx-5" exact>ACTIVE</Nav.Link>
        <Nav.Link as={NavLink} to="/addProduct" className="text-white mx-5" exact>ADD</Nav.Link>
        <Nav.Link as={NavLink} to="/allOrders" className="text-white mx-5" exact>ORDERS</Nav.Link>
        <Nav.Link as={NavLink} to="/adminLogout" className="text-white mx-5" exact>LOGOUT</Nav.Link>
        
        
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>

		)
}


